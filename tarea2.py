'''
Contador de palabras: actualizar el script anterior que lea un archivo de texto y 
retorne las 10 palabras más repetidas. Usando clases, métodos, instancias y lo previamente visto en clase.
'''

class Contador:

    xfile = 0
    

    def __init__(self, filename) -> None:
        filename = filename
        self.xfile = self.openFile(filename=filename)
        

    def openFile(self, filename):
        try:
            return open(filename)
        except FileExistsError:
            return 0

    def wordCounted(self):
        wordsCounted = {}
        for line in self.xfile:
            line = line.strip().lower().split(' ')
            for word in line:
                if word == '':
                    continue
                if word.endswith('.'):
                    word = word[0:-1]
                if word.endswith(','):
                    word = word[0:-1]
                wordsCounted[word] = wordsCounted.get(word, 0) + 1 

        return wordsCounted

    def tenMoreCuounted(self):
        wordsCounted = self.wordCounted()
        wordsCounted = sorted(wordsCounted.items(), key=lambda x: x[1], reverse=True)
        mostFoundWords = []
        for i in range(10):
            mostFoundWords.append(wordsCounted[i][0])
        return mostFoundWords

count = Contador('lorem.txt')
print(count.tenMoreCuounted())
