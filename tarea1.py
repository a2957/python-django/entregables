'''
Tarea 1 - Contador de palabras: 
crear un script que lea el nombre de un archivo de texto y retorne las 10 palabras más repetidas. 
Debería usar listas, diccionarios, ciclos, funciones, try/except.
'''

def mostFoundWords(filename):
    try:
        xfile = open(filename)
        wordsCounted = {}
        mostFoundWords = []
        for line in xfile:
            line = line.strip().lower().split(' ')
            for word in line:
                if word == '':
                    continue
                if word.endswith('.'):
                    word = word[0:-1]
                if word.endswith(','):
                    word = word[0:-1]
                wordsCounted[word] = wordsCounted.get(word, 0) + 1

        wordsCounted = sorted(wordsCounted.items(), key=lambda x: x[1], reverse=True)

        for i in range(10):
            mostFoundWords.append(wordsCounted[i][0])
        return mostFoundWords
    except FileExistsError as Error:
        return Error


filename = input('Nombre del archivo: ')
result = mostFoundWords(filename)
print(result)